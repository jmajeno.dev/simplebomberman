// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BomberGameMode.generated.h"

UCLASS(minimalapi)
class ABomberGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABomberGameMode();
};



