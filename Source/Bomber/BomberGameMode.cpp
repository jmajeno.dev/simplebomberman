// Copyright Epic Games, Inc. All Rights Reserved.

#include "BomberGameMode.h"
#include "BomberCharacter.h"
#include "UObject/ConstructorHelpers.h"

ABomberGameMode::ABomberGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
