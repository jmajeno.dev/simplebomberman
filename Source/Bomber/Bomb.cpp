// Fill out your copyright notice in the Description page of Project Settings.


#include "Bomb.h"
#include "Kismet/KismetSystemLibrary.h"
#include "BomberCharacter.h"

// Sets default values
ABomb::ABomb()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BombMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bomb"));
	
	BombMesh->SetSimulatePhysics(true);
	this->SetReplicates(true);
	
	RootComponent = BombMesh;

	
	
	SphereTraceRadius = 100.0f;
	SphereTraceDistance = 1000.0f;
}

// Called when the game starts or when spawned
void ABomb::BeginPlay()
{
	Super::BeginPlay();
	
	
}

void ABomb::ExplosionTimer()
{
	if (HasAuthority())
	{
		// Set a 10-second timer for the bomb to explode
		GetWorldTimerManager().SetTimer(ExplosionTimerHandle, this, &ABomb::Explode, 5.0f, false);
	}
}

// Called every frame
void ABomb::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABomb::Explode()
{
	FVector StartLocation = GetActorLocation();
	FVector EndLocation = StartLocation + GetActorForwardVector() * SphereTraceDistance;

	FHitResult HitResult;

	// Perform the Sphere Trace Multi
	TArray<FHitResult> HitResults;
	FCollisionQueryParams CollisionParams;
	CollisionParams.AddIgnoredActor(this); // Ignore the character itself

	bool bHit = GetWorld()->SweepMultiByChannel(HitResults, StartLocation, EndLocation, FQuat::Identity, ECC_Pawn, FCollisionShape::MakeSphere(SphereTraceRadius), CollisionParams);
	DrawDebugSphere(GetWorld(), StartLocation, SphereTraceRadius, 12, FColor::Green, false, 15, 0, 1);

	if (bHit)
	{
		// Iterate through the hit results
		for (const FHitResult& Hit : HitResults)
		{
			// Handle each hit result as needed
			AActor* HitActor = Hit.GetActor();
			
			GetActorsHit.Add(HitActor);
			
			// You can add code to respond to the hits, such as damaging objects, triggering events, etc.
			ABomberCharacter* BomberCharacter = Cast<ABomberCharacter>(HitActor);
			if(BomberCharacter)
			{
				BomberCharacter->DamageHP(30);
				FString YourMessage = TEXT("Cast succeded.");
				FColor TextColor = FColor::Green;
				float DisplayTime = 5.0f;

				GEngine->AddOnScreenDebugMessage(-1, DisplayTime, TextColor, YourMessage);
			}
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, TEXT("Hit"));
		}
	}
	ReturnBombToPool(this);
}

void ABomb::ReturnBombToPool(ABomb* BombInstance)
{
	if (BombInstance)
	{
		BombInstance->SetActorHiddenInGame(true);
		BombInstance->SetActorEnableCollision(false);
		UPrimitiveComponent* BombRoot = Cast<UPrimitiveComponent>(BombInstance->GetRootComponent());
		if (BombRoot)
		{
			// Enable physics simulation
			BombRoot->SetSimulatePhysics(false);
		}
	}
}

