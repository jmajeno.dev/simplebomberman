// Copyright Epic Games, Inc. All Rights Reserved.

#include "BomberCharacter.h"

#include "Bomb.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "Net/UnrealNetwork.h"


//////////////////////////////////////////////////////////////////////////
// ABomberCharacter

ABomberCharacter::ABomberCharacter()
{

	HP = 100;
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
		
	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f); // ...at this rotation rate

	// Note: For faster iteration times these variables, and many more, can be tweaked in the Character Blueprint
	// instead of recompiling to adjust them
	GetCharacterMovement()->JumpZVelocity = 700.f;
	GetCharacterMovement()->AirControl = 0.35f;
	GetCharacterMovement()->MaxWalkSpeed = 500.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 400.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named ThirdPersonCharacter (to avoid direct content references in C++)
}

void ABomberCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Add Input Mapping Context
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}
	InitilizeBombs(10);
}

//////////////////////////////////////////////////////////////////////////
// Input

void ABomberCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent)) {
		
		//Jumping
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Triggered, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);

		//Moving
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ABomberCharacter::Move);

		//Looking
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &ABomberCharacter::Look);

		EnhancedInputComponent->BindAction(BombAction, ETriggerEvent::Completed, this, &ABomberCharacter::Server_DropBomb);

	}

}

bool ABomberCharacter::Server_DropBomb_Validate()
{
	// Add validation logic here (e.g., return true for all cases)
	return true;
}

void ABomberCharacter::Server_DropBomb_Implementation()
{
	// Add your server authoritative logic here

	DropBomb();
}

void ABomberCharacter::InitilizeBombs(int32 PoolSize)
{
	for (int32 i = 0; i < PoolSize; i++)
	{
		FVector3d SpawnLocation = this->GetActorLocation() + FVector3d(0,0,1200);
		ABomb* NewBomb = GetWorld()->SpawnActor<ABomb>(Bomb, SpawnLocation, FRotator(0, 0, 0));
		if (NewBomb)
		{
			NewBomb->SetActorHiddenInGame(true);
			NewBomb->SetActorEnableCollision(false);
			UPrimitiveComponent* BombRoot = Cast<UPrimitiveComponent>(NewBomb->GetRootComponent());
			if (BombRoot)
			{
				// Disable physics simulation
				BombRoot->SetSimulatePhysics(false);
			}
			BombPool.Add(NewBomb);
		}
	}
}

ABomb* ABomberCharacter::GetBombFromPool()
{
	for (int i = 0; i < BombPool.Num(); ++i)
	{
		// Generate a random index within the pool size
		int RandomIndex = FMath::RandRange(0, BombPool.Num() - 1);

		ABomb* BombInstance = BombPool[RandomIndex];
		if (BombInstance->IsActorInitialized() && BombInstance->IsHidden())
		{
			return BombInstance;
		}
	}

	return nullptr;
}
void ABomberCharacter::DropBomb()
{
	FRotator SpawnRotation = FRotator(0, 0, 0);  // Set the initial rotation of the actor
	
	// Spawn the actor, replace with object pooling
	//ABomb* NewBomb = GetWorld()->SpawnActor<ABomb>(Bomb, SpawnLocation, SpawnRotation);
	ABomb* NewBomb = GetBombFromPool();
	
	if (NewBomb)
	{
		FVector3d SpawnLocation = GetActorLocation() + FVector3d(0,0,1200); // Set the initial location of the actor
		
		NewBomb->SetActorHiddenInGame(false);
		NewBomb->SetActorEnableCollision(true);
		NewBomb->SetActorLocation(SpawnLocation);
		UPrimitiveComponent* BombRoot = Cast<UPrimitiveComponent>(NewBomb->GetRootComponent());
		if (BombRoot)
		{
			// Enable physics simulation
			BombRoot->SetSimulatePhysics(true);
		}
		
		// The actor has been successfully spawned
		// You can further configure or interact with the spawned actor.
		FString YourMessage = TEXT("SpawnBomb() has succuceded.");
		FColor TextColor = FColor::Green;
		FVector2D ScreenLocation(100, 100);
		float DisplayTime = 5.0f;

		GEngine->AddOnScreenDebugMessage(-1, DisplayTime, TextColor, YourMessage);
		NewBomb->ExplosionTimer();

	}
	else
	{
		// Failed to spawn the actor
		FString YourMessage = TEXT("SpawnBomb() has failed.");
		FColor TextColor = FColor::Green;
		FVector2D ScreenLocation(100, 100);
		float DisplayTime = 5.0f;

		GEngine->AddOnScreenDebugMessage(-1, DisplayTime, TextColor, YourMessage);
	}
}

void ABomberCharacter::FireBullet()
{
	
}

void ABomberCharacter::Move(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	
		// get right vector 
		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		// add movement 
		AddMovementInput(ForwardDirection, MovementVector.Y);
		AddMovementInput(RightDirection, MovementVector.X);
	}
}

void ABomberCharacter::Look(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}

int32 ABomberCharacter::GetHP() const
{
	return HP;
}

void ABomberCharacter::DamageHP(int32 Damage)
{
	HP = HP - Damage;

	if (HP <= 0)
	{
		Destroy();
	}
}
void ABomberCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	// Replicate the HP variable
	DOREPLIFETIME(ABomberCharacter, HP);
}